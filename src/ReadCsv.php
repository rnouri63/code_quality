<?php

namespace Cesi\Msi\Archi;

require dirname(__DIR__) . '/vendor/autoload.php';

use Cesi\Msi\Archi\Command\ReadCsvCommand;
use Symfony\Component\Console\Application;

$application = new Application('read:csv', '1.4.1');
$command = new ReadCsvCommand();
$application->add($command);

$application->setDefaultCommand($command->getName(), true);
$application->run();
