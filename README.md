# Code Quality

- Parcourir un fichier CSV qui contient des urls.
- Pour chaque URL, récupérer les entetes de site associé.
- Afficher les en-têtes 'cache_control', 'content_type' et 'expires' si elles existent.

# Etapes à suivre 

- Etape 1 : Lancer docker
    - Dans le terminal, aller dans le dossier alloué au projet.
    - `docker-compose up -d`
- Etape 2 : Executer la commande
    - `docker-compose exec app php fileReader/read.php <filepath> [-s separator]` (séparateur optionnel)

# Version de la commande

- Etape 1 : Lancer docker
    - Dans le terminal, aller dans le dossier alloué au projet.
    - `docker-compose up -d`
- Etape 2 : Executer la commande
    - `docker-compose exec app php fileReader/read.php --version` qui va vous renvoyer la version actuelle
