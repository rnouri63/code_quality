<?php

namespace Cesi\Msi\Archi\Tests;

use PHPUnit\Framework\TestCase;
use Cesi\Msi\Archi\FileReader;

final class fileReaderUnitTest extends TestCase
{
    private static FileReader $fileReader;

    public static function setUpBeforeClass(): void
    {
        self::$fileReader = new FileReader("skcdxkvijsdvdfv.csv", ",");
    }

    public function testParseRepeatedHeaders()
    {
        $input = [
            'Content-Type' => [
                'toto',
                'titi',
            ],
            'Expires' => '',
            'Cache-Control' =>  '',
        ];

        $expected = [
            'Content-Type' => "toto,titi",
            'Expires' => '',
            'Cache-Control' =>  ''
        ];

        $actual = self::$fileReader->parseHeaders($input);

        $this->assertSame($expected, $actual);
    }
}
